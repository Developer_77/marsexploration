# Design Document
There is no separation by groups, all entered to app-users
<br>User can use the app without any restriction.

The following actions which might be performed by user shown here:
https://miro.com/app/board/o9J_lAlYXD8=/
<h2>The first glimpse at the project</h2>
This is how looks the Rover sector now(still in development):
https://miro.com/app/board/o9J_l_rwyR0=/
<h2>Structure of project</h2>
The Structure of project is very simple, all you need to know that the Android App consists of two types of implementation:
<br>The first one is Back-end. The Back-End of my project consists of the following classes: <b>MyApplication</b>(the place where I store all data), <b>Rover</b>(model of data), <b>Satellite</b>(model of data), <b>Lander</b>(model of data), 
<b>RecycleView</b>, <b>Adaptor for RecycleView</b>, <b>Activity2</b>(activity for each item in RecycleView), <b>MainActivity</b>.
The Front-End part is layouts that contains some graphical interfaces for user.
And, of course, the project consists some default packages for working on the Android platform.


<h2> Milestones</h2>

- Create the Rover's sector, Satellite's sector, Lander's sector
- Code a RecycleView, Adapter for RecycleView, layouts
- Implement all key functions
- Upgrade the GUI of App
