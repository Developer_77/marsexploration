package com.example.marsexploration;

public interface Spacecraft {
    String getName();
    String getDateOfLanding();
    String getImageURL();
    String getMode();
    String getOperator();
    String getDurationOfMission();
}
