package com.example.marsexploration;

import androidx.annotation.NonNull;

import java.net.URL;

public class Rover implements Spacecraft{
    private String emblem;
    private String name;
    private String DateOfLanding;
    private String imageURL;
    private String flag;
    private String Mode;
    private String operator;
    private String durationOfMission;
    private String pageURL;
    private String currentLocation;
    private String pressKit;
    private String ImagesData;

    public Rover(String name, String dateOfLanding, String imageURL, String mode, String durationOfMission,
                 String operator, String pageURL,String currentLocation,String pressKit,String ImagesData,String flag,String emblem) {
        this.emblem=emblem;
        this.name = name;
        this.DateOfLanding = dateOfLanding;
        this.imageURL = imageURL;
        this.flag=flag;
        this.Mode = mode;
        this.operator=operator;
        this.durationOfMission=durationOfMission;
        this.pageURL=pageURL;
        this.currentLocation=currentLocation;
        this.pressKit=pressKit;
        this.ImagesData=ImagesData;
    }

    @Override
    public String toString() {
        return "Rover{" +
                "name='" + name + '\'' +
                ", DateOfLanding='" + DateOfLanding + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", Mode='" + Mode + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfLanding() {
        return DateOfLanding;
    }

    public void setDateOfLanding(String dateOfLanding) {
        DateOfLanding = dateOfLanding;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getMode() {
        return Mode;
    }

    public void setMode(String mode) {
        Mode = mode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getDurationOfMission() {
        return durationOfMission;
    }

    public void setDurationOfMission(String durationOfMission) {
        this.durationOfMission = durationOfMission;
    }

    public String getPageURL() {
        return pageURL;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getPressKit() {
        return pressKit;
    }

    public void setPressKit(String pressKit) {
        this.pressKit = pressKit;
    }

    public String getImagesData() {
        return ImagesData;
    }

    public void setImagesData(String imagesData) {
        ImagesData = imagesData;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getEmblem() {
        return emblem;
    }

    public void setEmblem(String emblem) {
        this.emblem = emblem;
    }
}
